import { Component, OnInit } from '@angular/core';
import { Message } from '../message';
import { Helpers } from '../helpers';
import { Cipher } from '../cipher';

@Component({
  selector: 'encrypt-form',
  templateUrl: './encrypt-form.component.html',
  styleUrls: ['./encrypt-form.component.css']
})
export class EncryptFormComponent {
  title = "Cifrar";
  model = new Message();
  submitted = false;

  onSubmit() {
    this.submitted = true;
    this.model.encryptedText = Cipher.cipher(
      this.model.plainText,
      this.model.key,
      this.model.prime
    );
  }
}
