export class Message {
    constructor(
        public plainText?: string,
        public encryptedText?: string,
        public key?: number,
        public prime?: number
    ) { }
}
