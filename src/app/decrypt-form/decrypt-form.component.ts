import { Component, OnInit } from '@angular/core';
import { Cipher } from '../cipher';
import { Message } from '../message';

@Component({
  selector: 'decrypt-form',
  templateUrl: './decrypt-form.component.html',
  styleUrls: ['./decrypt-form.component.css']
})
export class DecryptFormComponent {
  title = "Descifrar";
  model = new Message();
  submitted = false;

  onSubmit() {
    this.submitted = true;
    this.model.plainText = Cipher.decipher(
      this.model.encryptedText,
      this.model.key,
      this.model.prime
    );
  }
}
