import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecryptFormComponent } from './decrypt-form.component';

describe('DecryptFormComponent', () => {
  let component: DecryptFormComponent;
  let fixture: ComponentFixture<DecryptFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecryptFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecryptFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
