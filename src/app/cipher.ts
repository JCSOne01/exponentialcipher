import { Helpers } from "./helpers";

export class Cipher {

    public static cipher(str: string, key: number, mod: number): string {
        var result = '';
        var groups = Helpers.splitInGroups(this.normalize(str), 2);
        for (var i = 0; i < groups.length; i++) {
            result += this.modPow(this.numericValue(groups[i]), key, mod);
        }
        return result;
    }

    public static decipher(str: string, key: number, mod: number): string {
        var result = '';
        var decoded = '';
        var d = this.solveCongruence(key, 1, this.phi(mod));
        var groups = Helpers.splitInGroups(this.normalize(str), 4);
        for (var i = 0; i < groups.length; i++) {
            decoded += this.modPow(parseInt(groups[i]), d, mod);
        }

        var error = false;
        var decodedGroups = Helpers.splitInGroups(decoded, 2);
        for (var i = 0; i < decodedGroups.length; i++) {
            let letter = Helpers.abecedaryValue(decodedGroups[i]);
            if (!letter) {
                error = true;
                break;
            }
            result += letter;
        }

        return !error ? result : 'El mensaje (' + this.normalize(str) + ') no fue cifrado usando la llave (' + key + ') con primo (' + mod + '). El resultado obtenido con los datos dados es: ' + decoded;
    }

    public static modPow(b: number, p: number, m: number): string {
        var x: number = 1;
        var pot = this.module(b, m);
        let binaryPot = p.toString(2);

        for (var i = binaryPot.length - 1; i >= 0; i--) {

            if (binaryPot[i] === '1')
                x = this.module(x * pot, m);
            pot = this.module(pot * pot, m);
        }
        return Helpers.padLeft(x.toString(), 4, '0');
    }

    public static solveCongruence(a: number, b: number, m: number): number {
        var answer: number;

        // var gcd = GCD(new[] { a, m });
        //if (gcd != 1)
        //{
        //    Console.WriteLine("No tiene Solución");
        //    return -1;
        //}

        if (b == 0) {
            return 0;
        }

        if (m < a) {
            a = this.module(a, m);
            b = this.module(b, m);
        }

        if (a == 1) {
            return b;
        }

        answer = this.solveCongruence(m, -b, a);

        return ((answer * m) + b) / a;
    }

    public static module(x: number, y: number): number {
        var module = x % y;

        return module < 0 ? y + module : module;
    }

    public static phi(n: number): number {
        var x = 0;
        for (var i = 1; i <= n; i++) {
            if (this.gcd(n, i) == 1) {
                x++;
            }
        }
        return x;
    }

    public static gcd(x: number, y: number): number {
        x = Math.abs(x);
        y = Math.abs(y);
        if (y > x) { var temp = x; x = y; y = temp; }
        while (true) {
            if (y == 0) return x;
            x %= y;
            if (x == 0) return y;
            y %= x;
        }
    }

    public static normalize(str: string): string {
        var target = str.replace(/\s/g, '');
        return target.length % 2 == 0 ? target.toUpperCase() : (target + 'X').toUpperCase();
    }

    public static numericValue(str: string): number {
        var number = '';
        for (var i = 0; i < str.length; i++) {
            number += Helpers.abecedaryKey(str[i]);
        }
        return parseInt(number);
    }
}
