import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule }   from '@angular/forms';
import { EncryptFormComponent } from './encrypt-form/encrypt-form.component';
import { DecryptFormComponent } from './decrypt-form/decrypt-form.component';

@NgModule({
  declarations: [
    AppComponent,
    EncryptFormComponent,
    DecryptFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
