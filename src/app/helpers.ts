export class Helpers {
    private static abecedary = new Map<string, string>(
        [
            ['00', 'A'],
            ['01', 'B'],
            ['02', 'C'],
            ['03', 'D'],
            ['04', 'E'],
            ['05', 'F'],
            ['06', 'G'],
            ['07', 'H'],
            ['08', 'I'],
            ['09', 'J'],
            ['10', 'K'],
            ['11', 'L'],
            ['12', 'M'],
            ['13', 'N'],
            ['14', 'Ñ'],
            ['15', 'O'],
            ['16', 'P'],
            ['17', 'Q'],
            ['18', 'R'],
            ['19', 'S'],
            ['20', 'T'],
            ['21', 'U'],
            ['22', 'V'],
            ['23', 'W'],
            ['24', 'X'],
            ['25', 'Y'],
            ['26', 'Z']]);

    public static abecedaryKey(val: string): string {
        // return this.padLeft((val.charCodeAt(0) % 32 - 1).toString(), 2, '0');
        return Array.from(this.abecedary.entries()).find((e) => e[1] === val)[0];
    }

    public static abecedaryValue(key: string): string {
        return this.abecedary.get(key);
    }

    public static padLeft(str: string, len: number, pad: string): string {
        return (len <= str.length) ? str : this.padLeft(pad + str, len, pad);
    }

    public static splitInGroups(str: string, size: number): Array<string> {
        return str.match(new RegExp('.{1,' + size + '}', 'g'));
    }
}
